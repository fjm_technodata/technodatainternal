SELECT AVG(colMedian) as Median
FROM 
(
SELECT colMedian, RANK() OVER(order by colMedian) as Rank1, 
RANK() OVER(order by colMedian desc) as Rank2
 FROM
( 
Select Distinct YourCol as colMedian  --YourCol is the columns of which you want to calculate the Median
from YourTable --and Condition
) internal 

) data 
 WHERE Rank1 = Rank2 or Rank1 = Rank2+1 or Rank1 = Rank2 - 1
 