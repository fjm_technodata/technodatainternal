DECLARE @TotalCount2  int 
SET @TotalCount2  =  (SELECT COUNT(DISTINCT(FRID)) from FJCLDB1.FJMCustomerData.ProfileReport.FRIDWithAgInvolvementCode
WHERE AgInvolvementCode in (@SingleQuote.Request.SelProfileReport1~)--('BC') --
);
SELECT * from (
 SELECT Northeastern
--,AgInvolvementCode
,count(distinct(FRID)) as Counts 
,@TotalCount2 as UniqueCounts
,ROUND((COUNT(DISTINCT(FRID))/CAST(@TotalCount2 AS FLOAT)),4) AS Perc
,NortheasternSort
from [ProfileReport].[RgnNortheasternGroupWithSort]
WHERE AgInvolvementCode in (@SingleQuote.Request.SelProfileReport1~)--('BC') --
group by Northeastern,NortheasternSort

UNION ALL
SELECT Northeastern,SUM(Counts) as Counts,UniqueCounts, Perc, NortheasternSort
FROM 
(
 SELECT 'Total' as Northeastern
,count(distinct(FRID)) as Counts
,@TotalCount2 as UniqueCounts
,ROUND((COUNT(DISTINCT(FRID))/CAST(@TotalCount2 AS FLOAT)),4) AS Perc
,'9' as NortheasternSort
from [ProfileReport].[RgnNortheasternGroupWithSort]
WHERE AgInvolvementCode in (@SingleQuote.Request.SelProfileReport1~)--('BC') --
)M
Group by M.Northeastern, M.UniqueCounts, M.Perc, M.NortheasternSort
)X1
ORDER BY NortheasternSort
